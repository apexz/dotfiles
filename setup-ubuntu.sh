#!/bin/bash

# Adding repositories
sudo add-apt-repository -y ppa:papirus/papirus
sudo add-apt-repository -y ppa:aguignard/ppa
sudo add-apt-repository -y ppa:dawidd0811/neofetch
sudo add-apt-repository -y ppa:linrunner/tlp

# Updating sources 
sudo apt update -y
sudo apt dist-upgrade -y

# Installing software
sudo apt install -y git
sudo apt install -y vim
sudo apt install -y neofetch
sudo apt install -y fonts-font-awesome
sudo apt install -y arc-theme arc-icons
sudo apt install -y i3
sudo apt install -y i3blocks
sudo apt install -y libxcb-xrm-dev
sudo apt install -y lxappearance
sudo apt install -y compton
sudo apt install -y feh
sudo apt install -y arandr
sudo apt install -y pavucontrol
sudo apt install -y thunar
sudo apt install -y rofi
sudo apt install -y papirus-icon-theme
sudo apt install -y dconf-editor
sudo apt install -y remmina
#sudo apt install -y ttf-mscorefonts-installer
sudo apt install -y cmake
sudo apt install -y python3-pip
sudo apt install -y libreoffice
sudo apt install -y libreoffice-style-papirus filezilla-theme-papirus 
sudo apt install -y htop
sudo apt install -y curl
sudo apt install -y filezilla
sudo apt install -y calibre
sudo apt install -y xbacklight pavucontrol
sudo apt install -y tlp tlp-rdw tp-smapi-dkms acpi-call-dkms
sudo apt install -y powertop
sudo apt install -y traceroute

cd Downloads
sudo wget http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb
sudo dpkg -i ttf-mscorefonts-installer_3.6_all.deb

# Updating the fonts cache
sudo fc-cache -f -v

# >_setup i3-gaps
#sudo apt install -y libxcb-xrm-dev libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev autoconf
#git clone https://www.github.com/Airblader/i3 i3-gaps
#cd i3-gaps
#autoreconf --force --install
#rm -rf build/
#mkdir -p build && cd build/
#../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
#make
#sudo make install
#sudo mv i3-gaps ~
#cd resource
# setup pywal
#sudo python3 get-pip.py
#sudo pip3 install pywal
# clear repositories
#sudo apt autoremove -y

echo "Finished"
echo "Please restart the machine."



