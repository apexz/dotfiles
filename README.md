# dotfiles

A collection of my .dotfiles for my Linux machine.

OS: Ubuntu 18.04
WM: i3

To be done:
- [ ] Installation script
- [ ] i3 config
- [ ] .vimrc
